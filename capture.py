import os
import cv2

IMAGE_DIR = './images/'

# Create the image directory if it does not exist yet.
if not os.path.isdir(IMAGE_DIR):
  os.makedirs(IMAGE_DIR)

# Determine the name of the new image.
imageId = len(os.listdir(IMAGE_DIR)) + 1
imageName = IMAGE_DIR + '{:05}.jpg'.format(imageId)

# Save the webcam image.
camera = cv2.VideoCapture(0)
return_value,image = camera.read()
cv2.imwrite(imageName,image)
camera.release()
